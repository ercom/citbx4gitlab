
SCRIPT='
echo "DOMAIN=$DOMAIN"
echo "WEBHOOK_URL=$WEBHOOK_URL"
echo "IMAGE_NAME=$CITBX_JOB_IMAGE_NAME"
echo "$CITBX_JOB_SCRIPT"
'

case "$CI_JOB_NAME" in
    rubocop)
        run_check \
            -m "^DOMAIN=$" \
            -m "^WEBHOOK_URL=$" \
            -m "^IMAGE_NAME=$" \
            -M "^echo Hello World$" \
            -- "$SCRIPT"
    ;;
    rspec)
        run_check \
            -m "^DOMAIN=$" \
            -m "^WEBHOOK_URL=https://my-webhook.example.com$" \
            -m "^IMAGE_NAME=ruby:2.4$" \
            -M "^echo Hello World$" \
            -- "$SCRIPT"
    ;;
    capybara)
        run_check \
            -m "^DOMAIN=$" \
            -m "^WEBHOOK_URL=$" \
            -m "^IMAGE_NAME=ruby:2.4$" \
            -m "^echo Hello World$" \
            -- "$SCRIPT"
    ;;
    karma)
        run_check \
            -m "^DOMAIN=example.com$" \
            -m "^WEBHOOK_URL=$" \
            -m "^IMAGE_NAME=ruby:2.4$" \
            -m "^echo Hello World$" \
            -- "$SCRIPT"
    ;;
    '')
        run_check \
            -m "^capybara karma rspec rubocop$" \
            -- 'echo ${CITBX_JOB_LIST[*]}'
    ;;
esac
